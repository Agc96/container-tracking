"""
Constantes comunes a todos los componentes del proyecto Container Tracking.
"""

#: Indica el formato por defecto para las fechas.
FORMAT_DATE = "%d/%m/%Y"
#: Indica el formato por defecto para las horas.
FORMAT_TIME = "%H:%M"
#: Indica el formato por defecto para las fechas y horas.
FORMAT_DATETIME = FORMAT_DATE + " " + FORMAT_TIME
