"""
Conjunto de utilidades comunes a todos los componentes del proyecto
Container Tracking.
"""

import logging
import os
from logging.handlers import SMTPHandler

import psycopg2
from psycopg2.extras import DictCursor

from .errors import CtConfigError

def get_database(username, password):
    """
    Realiza una conexión a la base de datos PostgreSQL común a los
    componentes del proyecto Container Tracking.
    """
    # Verificar que el usuario y contraseña son correctos
    if username is None or password is None:
        msg = ("No se encontraron los datos de conexión a base de datos. "
                "Verifique que se hayan configurado las variables de "
                "entorno correctamente.")
        raise CtConfigError(msg)
    # Crear el objeto de conexión a base de datos
    dbhost = os.getenv("DB_HOST", "localhost")
    dbname = os.getenv("DB_NAME", "tracking")
    return psycopg2.connect(cursor_factory=DictCursor, host=dbhost,
                            dbname=dbname, user=username,
                            password=password)

def get_logger(logname, filename):
    """
    Inicializa un logger para un componente del proyecto Container
    Tracking. Cada logger tiene dos handlers:
    
    - El primero usa un archivo .log donde se colocan todos los eventos
      relevantes de cada componente.
    - El segundo envía un correo electrónico al administrador del proyecto,
      y solo se enviarán los errores y excepciones que ocurran en algún
      componente.
    """
    logger = logging.getLogger(logname)
    logger.setLevel(logging.INFO)
    # Crear los handlers del logger
    file_handler = logging.FileHandler(filename)
    file_handler.setLevel(logging.INFO)
    mail_handler = get_mail_handler(logname)
    mail_handler.setLevel(logging.ERROR)
    # Configurar formato del logger
    formatter = logging.Formatter("[%(levelname)s %(asctime)s] %(message)s")
    file_handler.setFormatter(formatter)
    mail_handler.setFormatter(formatter)
    # Añadir handler al logger
    logger.addHandler(file_handler)
    logger.addHandler(mail_handler)
    return logger

def get_mail_handler(logname):
    """
    Inicializa un handler de correo electrónico, para uso del logger de un
    componente del proyecto Container Tracking.
    """
    # Verificar que el usuario y contraseña son correctos
    from_user = os.getenv("MAIL_USER")
    from_pass = os.getenv("MAIL_PASS")
    if from_user is None or from_pass is None:
        msg = ("No se encontraron los datos de correo electrónico. "
                "Verifique que se hayan configurado las variables de "
                "entorno correctamente.")
        raise CtConfigError(msg)
    # Crear el servicio de correos electrónicos SMTP
    to_email = os.getenv("MAIL_TO", from_user)
    subject = "[Container Tracking] Error en {0}".format(logname)
    return SMTPHandler("smtp.gmail.com", from_user, to_email, subject,
                        credentials=(from_user, from_pass), secure=())

if __name__ == "__main__":
    from dotenv import load_dotenv
    load_dotenv()
    # Probar el servicio de correo electrónico
    logger = get_logger("Componente de prueba", "test.log")
    logger.info("Ejemplo de mensaje de información")
    logger.error("Ejemplo de mensaje de error")
