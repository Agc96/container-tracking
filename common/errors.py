class CtError(Exception):
    """
    Excepción común a todos los componentes del proyecto Container Tracking.
    """
    pass

class CtConfigError(CtError):
    """
    Excepción que indica un error en la configuración de un componente del proyecto
    Container Tracking.
    """
    pass
