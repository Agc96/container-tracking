from enum import IntEnum

class CtEnum(IntEnum):
    """
    Enumerado base para uso en los componentes del proyecto Container Tracking.
    """

    def __new__(cls, value, desc):
        obj = int.__new__(cls, value)
        obj._value_ = value
        obj.desc = desc
        return obj

class CtContainerStatus(CtEnum):
    """
    Enumerado que contiene estados de contenedores marítimos.
    """

    #: Indica que el contenedor está pendiente de procesarse por el Web
    #: Scraper.
    PENDING = (1, 'Pendiente')

    #: Indica que el contenedor fue procesado por el Web Scraper, y está listo
    #: para que se pueda predecir su tiempo de llegada.
    PROCESSED = (2, 'Procesado')

    #: Indica que el contenedor fue procesado por el Web Scraper y su fecha de
    #: llegada ha sido estimada por el modelo de predicción. Un contenedor con
    #: este estado puede ser procesado de nuevo por el Web Scraper para
    #: actualizar sus datos.
    PREDICTED = (3, 'Procesado y estimado')

    #: Indica que el procesamiento del contenedor ha finalizado. El Web Scraper
    #: no debe procesar este contenedor de nuevo hasta que el usuario vuelva a
    #: registrar dicho contenedor, en otra fecha y con otro ID.
    FINISHED = (4, 'Finalizado')

    #: Indica que hubo un error al extraer la información del contenedor, por
    #: lo que el Web Scraper debe volver a intentar la extracción después de
    #: un tiempo determinado.
    ERROR_RETRY = (-1, 'Error al extraer la información')

    #: Indica que la empresa naviera notificó un error al extraer la
    #: información del contenedor, por lo que no se debe intentar extraer de
    #: nuevo.
    ERROR_ABORT = (-2, 'Error de la empresa naviera')

class CtVehicle(CtEnum):
    """
    Enumerado que contiene tipos de vehículos por los que un contenedor
    marítimo puede desplazarse en un movimiento en específico.
    """

    #: Indica que no se sabe en qué vehículo se está trasportando el
    #: contenedor.
    UNKNOWN = (0, 'Sin datos')

    #: Indica que el contenedor se está trasportando vía marítima, a través de
    #: un buque.
    VESSEL  = (1, 'Buque')

    #: Indica que el contenedor se está trasportando vía terrestre, a través de
    #: un camión.
    TRUCK   = (2, 'Camión')

    #: Indica que el contenedor se está trasportando vía terrestre, a través de
    #: un tren.
    TRAIN   = (3, 'Tren')
