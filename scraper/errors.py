from common.errors import CtConfigError, CtError

class CtScraperError(CtError):
    pass

class CtScraperConfigError(CtConfigError, CtScraperError):
    pass

class CtScraperAssertError(CtScraperError):
    pass

class CtScraperTimeoutError(CtScraperError):
    pass
