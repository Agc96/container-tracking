"""
Constantes a usarse en el Web Scraper para el proyecto Container Tracking.
"""

import os

from .enums import CtScraperAssertTypes

#: Valor por defecto para la ubicación del WebDriver para Google Chrome en
#: sistemas operativos Windows.
PATH_CHROME_WINDOWS = "C:\\WebDriver\\chromedriver.exe"

#: Valor por defecto para la ubicación del WebDriver para Google Chrome en
#: sistemas operativos GNU/Linux.
PATH_CHROME_LINUX = "/usr/local/bin/chromedriver"

#: Valor por defecto para la ubicación del WebDriver para Google Chrome,
#: independientemente del sistema operativo a usarse.
PATH_CHROME = PATH_CHROME_WINDOWS if os.name == "nt" else PATH_CHROME_LINUX

#: Valor por defecto para el comando de asumpción.
ASSERT_DEFAULT = CtScraperAssertTypes.NOT_REQUIRED

#: Cantidad de contenedores marítimos a extraer en una ronda.
ROUND_COUNT = 100

#: Duración (en segundos) en la que el Web Scraper se queda inactivo, antes de
#: volver a buscar contenedores marítimos.
SLEEP_TIME = 60

#: Tiempo máximo de espera (en segundos) para que cargue una página Web
#: solicitada.
PAGE_TIMEOUT = 60
