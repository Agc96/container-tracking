from ..consts import CtScraperConstants
from common.errors import ScraperCommandError

from .dom import ScraperDomExtractor

class CtScraperCommandProcessor:
    """
    Part of the Container Tracking Scraper. This class helps selecting which type should we use for
    selecting and saving Web elements and subelements in a tracking-related document.
    """

    def __init__(self, driver, logger, document, command, element=None):
        self.driver = driver
        self.logger = logger
        self.document = document
        self.command = command
        self.element = element or self.driver

    def process(self):
        """Process the provided element (or the WebDriver) according to the given command."""
        # Get process type
        command_type = self.command.get("type")
        if command_type is None:
            raise ScraperCommandError("Process type not found", self.command)
        # Select command processor based on process type
        if command_type in ("id", "class", "css", "tag", "name", "xpath"):
            return ScraperDomExtractor(self.command).process()
        raise ScraperCommandError("Process type {} does not exist".format(command_type), self.command)
    
    @staticmethod
    def process_from_list(driver, logger, document, commands, elements):
        """
        Crea una lista de procesadores de comandos del Web Scraper según cada comando de una lista
        del archivo de configuración.
        """
        for command in commands:
            # Get the index
            index = command.get("index")
            if not isinstance(index, int):
                raise ScraperCommandError("Child index command not found or not an integer", command)
            # Check if requested index exists in element list
            if abs(index) >= len(elements):
                required = command.get("required", ScraperConfig.DEFAULT_REQUIRED)
                logger.debug(ScraperCommandError.print_command(command))
                logger.debug("Element at index {} not found, returning {}.".format(index, required))
                return not required
            # Get element in index and process it
            result = ScraperCommandSwitcher(driver, logger, document, command, elements[index]).process()
            if result is not True:
                return result
        # Devolver True para indicar que el proceso fue exitoso
        return True
