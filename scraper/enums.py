from common.enums import CtEnum

class CtScraperResultTypes(CtEnum):
    """
    Enumerado para los resultados del proceso de extracción del Web Scraper
    del proyecto Container Tracking.
    """

    #: Indica que no hubo algún error al extraer la información del contenedor.
    NO_ERROR = (0, "Sin errores")

    #: Indica que hubo un error desconocido (excepción) al extraer la
    #: información del contenedor.
    ERROR_UNKNOWN = (1, "Error desconocido")

    #: Indica que no se recibió respuesta de la empresa naviera en el tiempo
    #: esperado, usualmente debido a pérdidas de conexión a Internet.
    ERROR_TIMEOUT = (2, "Sin conexión a Internet")

    #: Indica que al procesar un comando de extracción para el contenedor, se
    #: detectó que un elemento del DOM requerido para la extracción existe o
    #: no existe, dependiendo del caso.
    ASSERTION_FAILED = (3, "Fallo al procesar la información")

    #: Indica que ocurrió un error al procesar algún comando del archivo de
    #: configuración, sea para la empresa naviera asociada a un contenedor
    #: (JSON de configuración de extracción), o para el proyecto entero
    #: (variables de entorno o similares).
    ERROR_CONFIGURATION = (4, "Error en configuración")

    #: Hubo un error desconocido (excepción) al usar la librería de Selenium
    #: WebDriver. Este error permite al desarrollador observar lo que está
    #: fallando y corregir el código.
    #: FIXME: Ver qué es lo que causa este error en la instancia de EC2.
    ERROR_WEBDRIVER = (5, "Error desconocido")

class CtScraperAssertTypes(CtEnum):
    """
    Enumerado para los tipos de evaluaciones que puede tener un comando del
    Web Scraper del proyecto Container Tracking.
    """

    #: El elemento descrito por el comando no es requerido, si el elemento no
    #: existe en el navegador, simplemente se coloca None como valor, o se
    #: ignora la acción.
    NOT_REQUIRED = (0, "No requerido")

    #: Se debe asegurar que el valor del elemento descrito por el comando
    #: exista. Si el elemento no existe en el navegador, se debe volver a
    #: realizar la extracción.
    ASSERT_EXISTS_OR_RETRY = (1, "Asegurar que existe o volver a intentarlo")

    #: Se debe asegurar que el valor del elemento descrito por el comando
    #: exista. Si el elemento no existe en el navegador, se debe cancelar
    #: definitivamente la extracción.
    ASSERT_EXISTS_OR_ABORT = (2, "Asegurar que existe o cancelar extracción")

    #: Se debe asegurar que el valor del elemento descrito por el comando no
    #: exista. Si el elemento existe en el navegador, se debe volver a
    #: realizar la extracción.
    ASSERT_NOT_EXISTS_OR_RETRY = (3, "Asegurar que no existe o volver a intentarlo")

    #: Se debe asegurar que el valor del elemento descrito por el comando no
    #: exista. Si el elemento existe en el navegador, se debe cancelar
    #: definitivamente la extracción.
    ASSERT_NOT_EXISTS_OR_ABORT = (4, "Asegurar que no existe o cancelar extracción")

    #: TODO: Seguir explicando
    ASSERT_EXISTS_OR_RESTART = (5, "Asegurar que existe o reiniciar scraper")

    #: TODO: Seguir explicando
    ASSERT_NOT_EXISTS_OR_RESTART = (6, "Asegurar que no existe o reiniciar scraper")

class CtScraperCommandTypes(CtEnum):
    """
    Enumerado para los tipos de comandos de guardado del Web Scraper del
    proyecto Container Tracking.
    """

    #: Busca elementos del DOM según su identificador (atributo "id"). Un
    #: ejemplo sería `<div id="results">...</div>`, donde "results" vendría a
    #: ser el identificador del elemento.
    FIND_BY_ID = (1, "Encontrar elemento(s) por ID")

    #: Busca elementos del DOM según la clase del elemento. Un ejemplo sería
    #: `<div class="container">...</div>`, donde "container" vendría a ser la
    #: clase del elemento.
    FIND_BY_CLASS = (2, "Encontrar elemento(s) por clase")

    #: Busca elementos del DOM según la etiqueta HTML del elemento. Ejemplos
    #: de etiquetas son `<span>`, `<div>`, `<table>`, `<a>`, `<img>`, `<h1>`,
    #: `<input>`, etc. Puede ver una lista completa de etiquetas en:
    #: https://developer.mozilla.org/es/docs/HTML/HTML5/HTML5_lista_elementos
    FIND_BY_TAG = (3, "Encontrar elemento(s) por etiqueta HTML")

    #: Busca elementos del DOM según el nombre (atributo "name") del elemento.
    #: Un ejemplo sería `<input name="username">`, donde "username" vendría a
    #: ser el nombre del elemento.
    FIND_BY_NAME = (4, "Encontrar elemento(s) por nombre")

    #: Busca elementos del DOM según un selector CSS. TODO: Seguir explicando
    FIND_BY_CSS = (5, "Encontrar elemento(s) por selector CSS")

    #: Busca elementos del DOM según un selector XPath. TODO: Seguir explicando
    FIND_BY_XPATH = (6, "Encontrar elemento(s) por selector XPath")

    #: TODO: Seguir explicando
    SPLIT = (7, "Separar palabras según un caracter")

    #: TODO: Seguir explicando
    REGEX = (8, "Buscar una expresión regular")

    #: TODO: Seguir explicando
    SAVE = (9, "Guardar valor del elemento")

    #: TODO: Seguir explicando
    CLICK = (10, "Hacer clic en el elemento")

    #: TODO: Seguir explicando
    WRITE = (11, "Escribir texto en un elemento")

    #: TODO: Seguir explicando
    FIND_ALERT = (12, "Encontrar alerta del navegador")

    #: TODO: Seguir explicando
    FIND_FRAME = (13, "Encontrar frame de la página web")

    #: TODO: Seguir explicando
    ATTRIBUTE = (14, "Obtener atributo del elemento")

    #: TODO: Seguir explicando
    COMPARE = (15, "Comparar valor del elemento")

    #: TODO: Seguir explicando
    IMAGE = (16, "Procesar la imagen del elemento")

class CtScraperFormatTypes(CtEnum):
    """
    Enumerado para los tipos de formatos que puede tener un comando de guardado
    del Web Scraper del proyecto Container Tracking.
    """

    #: Dejar el texto descrito por el elemento como una cadena de caracteres.
    STRING = (0, "Sin cambios")

    #: Convertir el texto descrito por el elemento como un número entero (int).
    INTEGER = (1, "Convertir a entero")

    #: Convertir el texto descrito por el elemento como un número en punto
    #: flotante (float).
    FLOAT = (2, "Convertir a punto flotante")

    #: Convertir el texto descrito por el elemento como un objeto de fecha
    #: (date), asumiendo que la fecha descrita está en UTC.
    DATE_UTC = (3, "Convertir a fecha UTC")

    #: Convertir el texto descrito por el elemento como un objeto de hora
    #: (time), asumiendo que la hora descrita está en UTC.
    TIME_UTC = (4, "Convertir a hora UTC")

    #: Convertir el texto descrito por el elemento como un objeto de fecha y
    #: hora (datetime), asumiendo que la fecha y hora descrita está en UTC.
    DATETIME_UTC = (5, "Convertir a fecha y hora UTC")

    #: Convertir el texto descrito por el elemento como un objeto de fecha
    #: (date), tomándose en cuenta la diferencia horaria del Perú respecto al
    #: UTC (-5 horas).
    DATE_LOCAL = (6, "Convertir a fecha local")

    #: Convertir el texto descrito por el elemento como un objeto de hora
    #: (time), tomándose en cuenta la diferencia horaria del Perú respecto al
    #: UTC (-5 horas).
    TIME_LOCAL = (7, "Convertir a hora local")

    #: Convertir el texto descrito por el elemento como un objeto de fecha y
    #: hora (datetime), tomándose en cuenta la diferencia horaria del Perú
    #: respecto al UTC (-5 horas).
    DATETIME_LOCAL = (8, "Convertir a fecha y hora local")

    #: Guardar el texto descrito por el elemento en el apartado de ubicación
    #: del contenedor.  Adicionalmente, realiza una búsqueda de las coordenadas
    #: de latitud y longitud para guardarlas en los datos del contenedor.
    LOCATION = (9, "Guardar como ubicación del contenedor")

    #: Guardar el texto descrito por el elemento en el apartado de estado del
    #: movimiento del contenedor. 
    STATUS = (10, "Guardar como estado del contenedor")

    #: Guardar el valor descrito por el comando en el apartado de vehículo del
    #: movimiento del contenedor. Puede ver los tipos de vehículos registrados
    #: en el enumerado :class:`common.enums.CtVehicle`.
    VEHICLE = (11, "Guardar como vehículo del contenedor")
