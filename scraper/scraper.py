import json
import os
import time

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.webdriver import WebDriver as Chrome

import common.utils as CtUtilities
import scraper.consts as CtScraperConstants
from common.enums import CtContainerStatus
from scraper.errors import CtScraperConfigError, CtScraperTimeoutError

class CtScraper:
    """
    Clase que controla el proceso de extracción del Web Scraper para el
    proyecto Container Tracking.
    """

    def __init__(self, carrier):
        self.carrier = carrier
        # TODO: Corregir base de datos y descomentar esto
        # self.config = carrier["config"]
        filename = "scraper/carrier/{0}.json".format(carrier["id"])
        with open(filename, "r", encoding="UTF-8") as file:
            self.config = json.load(file)
        # Inicializar el logger para esta empresa naviera
        logname  = "Web Scraper ({0})".format(carrier["name"])
        filename = "logs/scraper-{0}.log".format(carrier["id"])
        self.logger = CtUtilities.get_logger(logname, filename)
        # Inicializar la base de datos
        username = os.getenv("DB_SCRAPER_USER")
        password = os.getenv("DB_SCRAPER_PASS")
        self.database = CtUtilities.get_database(username, password)
        # Inicializar el WebDriver
        self.init_driver()
        # Inicializar los contadores
        self.total_counter = 0
        self.round_counter = 0
        self.fail_counter  = 0
        self.fail_backoff  = 1

    def init_driver(self):
        """
        Inicializa el WebDriver de Google Chrome, y antes de ello cierra el
        WebDriver anterior, si es que existe.
        """
        # Cerrar el WebDriver anterior
        try:
            self.driver.quit()
        except:
            pass
        # Inicializar las opciones de Google Chrome
        options = ChromeOptions()
        options.set_headless(True)
        # Inicializar el WebDriver
        self.driver = Chrome(executable_path=CtScraperConstants.PATH_CHROME,
                             options=options)
        self.driver.maximize_window()
        self.driver.set_page_load_timeout(CtScraperConstants.PAGE_TIMEOUT)

    def execute(self):
        """
        Ejecuta el proceso de extracción de información de los contenedores
        marítimos para el proyecto Container Tracking de forma periódica. El
        proceso de extracción es el siguiente:

        - Obtiene desde la base de datos una lista de contenedores marítimos.
          Los criterios para que los contenedores estén incluidos en esta lista
          están descritos en el método :meth:`~get_containers`.

        - Ejecuta los pasos descritos en el método :meth:`~process_container`
          y actualiza el estado del contenedor. Luego, el Web Scraper prosigue
          con el siguiente contenedor, y así sucesivamente hasta que ocurra
          alguna de las condiciones descritas a continuación.

        - Cuando haya ocurrido un error durante la extracción de un contenedor,
          o al terminarse la lista de contenedores marítimos obtenida en el
          primer paso, el Web Scraper quedará inactivo un tiempo (definido en
          :any:`SLEEP_TIME <scraper.consts.CtScraperConstants.SLEEP_TIME>`) y
          se reiniciará la instancia del WebDriver.
        """
        while True:
            try:
                # Obtener una lista de contenedores pendientes para procesar
                container = self.get_containers()
                if container:
                    result = self.process_container(container)
                    # TODO: Terminar esto
                else:
                    # Esperar un tiempo y volver a buscar contenedores
                    self.logger.info("Esperando 1 minuto para volver a extraer...")
                    time.sleep(CtScraperConstants.SLEEP_TIME)
            except Exception as ex:
                self.logger.exception("Excepción inesperada en <execute>")
                raise
            except KeyboardInterrupt:
                self.logger.warning("Finalizando el proceso de extracción...")
                break

    def get_containers(self):
        """
        Obtiene un contenedor marítimo pendiente para ser procesado por el Web
        Scraper. Los criterios para que el contenedor sea seleccionado son los
        siguientes:

        - Los contenedores deben ser trasportados por la misma empresa naviera
          asociada a este Web Scraper.

        - Solo se procesan aquellos contenedores con los estados
          :any:`PENDING <common.enums.CtContainerStatus.PENDING>` y
          :any:`ERROR_RETRY <common.enums.CtContainerStatus.ERROR_RETRY>`.

        - La cantidad máxima de contenedores para esta ronda está definida en
          :any:`ROUND_COUNT <scraper.consts.CtScraperConstants.ROUND_COUNT>`.
        """
        with self.database as conn:
            with conn.cursor() as cur:
                # Preparar la sentencia SQL
                statement = """ SELECT * FROM tracking_container
                                WHERE carrier_id = %s AND status_id IN %s
                                ORDER BY status_id, priority
                                LIMIT 1 """
                statuses = (CtContainerStatus.PENDING,
                            CtContainerStatus.ERROR_RETRY)
                params = (self.carrier["id"], statuses)
                # Ejecutar la sentencia SQL
                cur.execute(statement, params)
                return cur.fetchall()

    def process_container(self, container):
        """
        Extrae la información de un contenedor marítimo procesando los comandos
        de configuración indicados para la empresa naviera. El proceso de
        extracción es el siguiente:

        TODO: Terminar explicación
        """
        movements = self.get_movements(container)
        try:
            self.go_to_url()
            self.process_commands(container, "general")
            self.process_commands(container, "container")
            self.process_movements(container, movements)
        except Exception as ex:
            self.logger.exception("Excepción obtenida en <process_container>")
            return None
        return None

    def get_movements(self, container):
        """
        Obtiene la lista de movimientos anteriormente extraída de un contenedor
        marítimo.
        """
        with self.database as conn:
            with conn.cursor() as cur:
                sql = "SELECT * FROM tracking_movements WHERE movement_id = %s"
                cur.execute(sql, (container["id"],))
                return cur.fetchall()

    def go_to_url(self, container):
        """
        Indica al WebDriver que debe ir al sitio Web de la empresa naviera. Se
        chequea también si es que el sitio Web es un error de Google Chrome, ya
        que Selenium WebDriver por sí solo no permite determinar si la página
        cargada es de de un error HTTP.
        """
        # Obtener la URL del sitio Web
        try:
            link = self.config["general"]["url"]
        except KeyError as ex:
            msg = ("No se pudo encontrar la URL de la empresa naviera. "
                   "Verifique que el archivo de configuración esté correcto.")
            raise CtScraperConfigError(msg) from ex
        # Indicar al WebDriver para ir al sitio web
        try:
            self.driver.get(link.format(container=container["id"]))
        except TimeoutException as ex:
            msg = ("No se cargó la página Web de la empresa naviera en el "
                   "tiempo requerido. Verifique que el servidor tenga una "
                   "conexión estable a Internet.")
            raise CtScraperTimeoutError(msg) from ex
        # Verificar si es un error de Google Chrome
        error = self.driver.find_elements_by_css_selector("body.neterror")
        if len(error) > 0:
            msg = ("Al intentar cargar la página Web de la empresa naviera,"
                   "se obtuvo una página de error de Google Chrome. Verifique "
                   "que el servidor tenga una conexión estable a Internet.")
            raise CtScraperTimeoutError(msg)

    def process_commands(self, container, parent_key):
        pass

    def process_movements(self, container, movements):
        pass
