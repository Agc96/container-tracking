import os
from multiprocessing import Process

import common.utils as CtUtilities

from .scraper import CtScraper

def execute(carrier):
    """
    Función auxiliar que inicializa, para cada empresa naviera, un subproceso
    encargado de ejecutar el Web Scraper. Véase :class:`.scraper.CtScraper`.
    """
    CtScraper(carrier).execute()

def main():
    """
    Función principal del Web Scraper del proyecto Container Tracking, para la
    extracción de información de contenedores marítimos. Esta función realiza
    lo siguiente:

    - Obtiene desde base de datos la lista de empresas navieras.
    - Crea un subproceso por cada empresa. Cada subproceso ejecutará una
      instancia del Web Scraper exclusivamente para la interacción con el sitio
      Web de la empresa naviera.

    Puede presionar `Ctrl + C` para terminar todos los subprocesos.
    """
    # Obtener la conexión a base de datos
    username = os.getenv("DB_SCRAPER_USER")
    password = os.getenv("DB_SCRAPER_PASS")
    database = CtUtilities.get_database(username, password)
    # Obtener los datos de las empresas navieras desde base de datos
    with database as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM tracking_enterprise WHERE carrier = true")
            carriers = cur.fetchall()
    # Abrir un subproceso por cada empresa naviera
    processes = []
    for carrier in carriers:
        p = Process(target=execute, args=(carrier,))
        processes.append(p)
        p.start()
    # Esperar a que terminen todos los subprocesos
    try:
        for p in processes:
            p.join()
    except KeyboardInterrupt:
        for p in processes:
            p.join()

if __name__ == "__main__":
    from dotenv import load_dotenv
    load_dotenv()
    main()
