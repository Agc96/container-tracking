-- Maersk
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('MNBU0071055', 1, 1, 73);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('MWCU6922679', 1, 1, 105);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('MSWU9086104', 1, 1, 29);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('MNBU3780183', 1, 1, 73);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('MRKU5777701', 1, 32, 553);
-- Hapag-Lloyd
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('UACU5281572', 2, 204, 42);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('UACU8318422', 2, 110, 180);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('UACU4792710', 2, 161, 78);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HLBU1705010', 2, 33, 180);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('UACU8352345', 2, 165, 1);
-- Evergreen
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HMCU9003090', 3, 50, 11);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HMCU9023310', 3, 50, 206);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HMCU9071319', 3, 50, 29);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HMCU9154964', 3, 50, 222);
INSERT INTO tracking_container (code, carrier_id, origin_id, destination_id) VALUES ('HMCU9187546', 3, 50, 144);
