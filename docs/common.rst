*****************
Elementos comunes
*****************

Constantes
==========

.. automodule:: common.consts
   :members:

Utilidades
==========

.. automodule:: common.utils
   :members:

Enumerados
==========

.. automodule:: common.enums
   :members: CtContainerStatus, CtVehicle

.. autoclass:: common.enums.CtEnum
   :members:

   .. automethod:: common.enums.CtEnum.find

Excepciones
===========

.. automodule:: common.errors
   :members:
