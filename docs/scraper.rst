***********
Web Scraper
***********

Proceso de extracción
=====================

.. automodule:: scraper.__main__
   :members:

.. automodule:: scraper.scraper
   :members:

Constantes
==========

.. automodule:: scraper.consts
   :members:

Enumerados
==========

.. automodule:: scraper.enums
   :members:

Excepciones
===========

.. automodule:: scraper.errors
   :members:
