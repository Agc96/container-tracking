.. Container Tracking documentation master file, created by
   sphinx-quickstart on Sat Oct 19 19:01:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*********************************************
Documentación del proyecto Container Tracking
*********************************************

El proyecto Container Tracking permite la extracción, predicción y la
visualización de contenedores marítimos usando técnicas de Web Scraping y
Aprendizaje de Máquina, brindando además un sistema de información Web para
el registro y visualización del estado de los contenedores.

Tabla de contenido
==================

.. toctree::
   :maxdepth: 2

   scraper.rst
   common.rst

Índices y tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
