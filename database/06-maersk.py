import logging
import time

from pymongo import MongoClient
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

# Define constants
MAERSK = 1
LIMIT = 150 #containers
TIMEOUT = 30 #seconds
SLEEP = 60 #seconds

# Initialize values
logging.basicConfig(filename="06-maersk.log", level=logging.INFO, format="[%(levelname)s %(asctime)s] %(message)s")
db = MongoClient().tracking
driver = webdriver.Firefox()

try:
    while True:
        # Find containers
        query = {
            "carrier": MAERSK,
            "found": None
        }
        containers = list(db.containers.find(query).limit(LIMIT))
        if len(containers) == 0:
            break
        for container in containers:
            # Go to page
            driver.get("https://www.maersk.com/tracking/#tracking/" + container["code"])
            # Try to find the origin points
            query = {
                "_id": container["_id"]
            }
            try:
                start = time.time()
                selector = (By.CLASS_NAME, "font--display-1--heavy")
                elements = WebDriverWait(driver, TIMEOUT).until(EC.visibility_of_all_elements_located(selector))
                # Save container as "found"
                db.containers.update_one(query, {
                    "$set": {
                        "origin": elements[0].text.strip(),
                        "destination": elements[1].text.strip(),
                        "found": True
                    }
                })
                logging.info("Container %s found! Use this as quick as you can.", container["code"])
                # Wait until timeout has completed
                end = time.time()
                time.sleep(TIMEOUT + start - end)
            except TimeoutException:
                # Save container as "not found"
                db.containers.update_one(query, {
                    "$set": {
                        "found": False
                    }
                })
                logging.info("Container %s was not found", container["code"])
        # Restart driver
        driver.quit()
        time.sleep(SLEEP)
        driver = webdriver.Firefox()
except KeyboardInterrupt:
    print()
except:
    logging.exception("Unknown exception occurred")
finally:
    driver.quit()
