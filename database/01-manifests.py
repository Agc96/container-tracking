import time
from datetime import datetime

from pymongo import MongoClient
from selenium import webdriver

# Configure MongoDB
database = MongoClient()["tracking"]
manifests = database["manifests"]

# Go to URL
driver = webdriver.Firefox()
driver.get("http://www.aduanet.gob.pe/aduanas/informao/HRMCFLlega.htm")
time.sleep(5)
# Set date
element = driver.find_element_by_name("CMc2_Fecha1")
element.clear()
element.send_keys("01/08/2019")
time.sleep(1)
# Submit
button = driver.find_elements_by_css_selector("input[type=button]")[0]
button.click()
time.sleep(5)

# Get table rows
table = driver.find_elements_by_tag_name("table")[3]
rows = table.find_elements_by_css_selector("tr:not(:first-child)")
# Get manifests
for row in rows:
    # Initialize values
    manifest = {
        "processed": False
    }
    cells = row.find_elements_by_tag_name("td")
    # Get manifest year and code
    text = cells[0].text.strip().split(" - ")
    manifest["year"] = 2000 + int(text[0])
    manifest["code"] = int(text[1])
    # Get manifest arrival date
    text = cells[1].text.strip()
    manifest["arrival"] = datetime.strptime(text, "%d/%m/%Y")
    # Get manifest discharge date
    text = cells[2].text.strip()
    try:
        manifest["discharge"] = datetime.strptime(text, "%d/%m/%Y")
    except:
        pass
    # Get ship name
    manifest["ship"] = cells[3].text.strip()
    # Upsert manifest
    query = {
        "year": manifest["year"],
        "code": manifest["code"]
    }
    update = {
        "$set": manifest
    }
    manifests.update_one(query, update, upsert=True)

# Close driver
driver.quit()
