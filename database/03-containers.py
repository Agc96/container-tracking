import logging
import time

from pymongo import MongoClient
from selenium import webdriver

# Initialize values
logging.basicConfig(filename="03-containers.log", level=logging.INFO, format="[%(levelname)s %(asctime)s] %(message)s")
db = MongoClient().tracking
driver = webdriver.Firefox()

try:
    # Find Maersk manifests
    query = {
        "ship": {
            "$regex": "MAERSK"
        }
    }
    select = {
        "year": True,
        "code": True,
        "ship": True
    }
    manifests = list(db.manifests.find(query, select))
    for manifest in manifests:
        # Find knowledges from Maersk manifests
        query = {
            "manifest": manifest["_id"],
            "processed": False
        }
        select = {
            "detail": True,
            "code": True
        }
        knowledges = list(db.knowledges.find(query, select))
        for knowledge in knowledges:
            # Go to knowledge URL
            driver.get(("http://www.aduanet.gob.pe/servlet/CRManDesc?CMc2_Anno={0[year]}&CMc2_Numero={0[code]}&"
                        "CMc2_NumDet={1[detail]}&CG_cadu=118&CMc2_TipM=mc&CMc2_viatra=1").format(manifest, knowledge))
            time.sleep(5)
            # Get containers table
            try:
                table = driver.find_elements_by_tag_name("table")[4]
                rows = table.find_elements_by_css_selector("tr:not(:first-child)")
                for row in rows:
                    cells = row.find_elements_by_tag_name("td")
                    # Get container code
                    code = cells[0].text.strip()
                    # Get container carrier
                    query = {
                        "code": code[:4].upper()
                    }
                    carrier = db.prefixes.find_one(query)
                    carrier = carrier["carrier"] if carrier else None
                    # Upsert container to database
                    query = {
                        "code": code
                    }
                    update = {
                        "$set": {
                            "code": code,
                            "carrier": carrier
                        },
                        "$addToSet": {
                            "knowledges": knowledge["_id"]
                        }
                    }
                    db.containers.update_one(query, update, upsert=True)
            except IndexError:
                logging.info("Knowledge {0[year]}-{0[code]}-{1[detail]} has no containers".format(manifest, knowledge))
            # Update knowledge
            query = {
                "_id": knowledge["_id"]
            }
            update = {
                "$set": {
                    "processed": True
                }
            }
            db.knowledges.update_one(query, update)
            time.sleep(5)
except KeyboardInterrupt:
    print()
except:
    logging.exception("Unknown exception occurred")
finally:
    driver.quit()
