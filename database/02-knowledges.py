import logging
import time
from datetime import datetime, timedelta

from pymongo import MongoClient
from selenium import webdriver

# Configure logging
logging.basicConfig(filename="02-knowledges.log", level=logging.INFO, format="[%(levelname)s %(asctime)s] %(message)s")
# Configure MongoDB
database = MongoClient()["tracking"]
manifests = database["manifests"]
knowledges = database["knowledges"]

# Get snapshot of manifests
query = {
    "processed": False
}
snapshot = list(manifests.find(query))

driver = webdriver.Chrome()
try:
    # Iterate through manifests
    for manifest in snapshot:
        # Go to URL
        driver.get("http://www.aduanet.gob.pe/cl-ad-itconsmanifiesto/manifiestoITS01Alias?accion=cargarFrmConsultaManifiesto&tipo=M")
        time.sleep(5)
        # Write year input
        year_input = driver.find_element_by_name("CMc1_Anno")
        year_input.clear()
        year_input.send_keys(manifest["year"])
        time.sleep(1)
        # Write manifest input
        elem = driver.find_element_by_name("CMc1_Numero")
        elem.clear()
        elem.send_keys(manifest["code"])
        time.sleep(1)
        # Submit
        button = driver.find_elements_by_css_selector("input[type=button]")[0]
        button.click()
        time.sleep(5)
        # Get tables
        tables = driver.find_elements_by_tag_name("table")

        # Get manifest information
        rows = tables[2].find_elements_by_tag_name("tr")
        # Replace manifest arrival date
        try:
            cells = rows[1].find_elements_by_tag_name("td")
            text = cells[1].text.strip()
            manifest["arrival"] = datetime.strptime(text, "%d/%m/%Y %H:%M") - timedelta(hours=-5)
        except IndexError:
            logging.warning("Could not find arrival date for manifest {0[year]}-{0[code]}".format(manifest))
        except ValueError:
            logging.warning("Could not parse arrival date {0} for manifest {1[year]}-{1[code]}".format(text, manifest))
        # Replace manifest discharge date
        try:
            cells = rows[2].find_elements_by_tag_name("td")
            text = cells[1].text.strip()
            manifest["discharge"] = datetime.strptime(text, "%d/%m/%Y %H:%M") - timedelta(hours=-5)
        except IndexError:
            logging.warning("Could not find discharge date for manifest {0[year]}-{0[code]}".format(manifest))
        except ValueError:
            logging.info("Could not parse discharge date {0} for manifest {1[year]}-{1[code]}".format(text, manifest))
        # Save manifest transition date
        try:
            cells = rows[7].find_elements_by_tag_name("td")
            text = cells[1].text.strip()
            manifest["transmission"] = datetime.strptime(text, "%d/%m/%Y %H:%M:%S") - timedelta(hours=-5)
        except IndexError:
            logging.info("Could not find transmission date for manifest {0[year]}-{0[code]}".format(manifest))
        except ValueError:
            logging.info("Could not parse transmission date {0} for manifest {1[year]}-{1[code]}".format(text, manifest))

        # Get knowledge list
        try:
            rows = tables[3].find_elements_by_css_selector("tr:not(:first-child)")
            for row in rows:
                # Initialize values
                knowledge = {
                    "manifest": manifest["_id"],
                    "processed": False
                }
                cells = row.find_elements_by_tag_name("td")
                # Get knowledge origin port
                knowledge["origin"] = cells[0].text.strip()
                # Get knowledge knowledge code
                knowledge["code"] = cells[2].text.strip()
                # Get knowledge detail number
                knowledge["detail"] = cells[4].text.strip()
                # Get knowledge consignee
                knowledge["consignee"] = cells[14].text.strip()
                # Get knowledge shipper
                knowledge["shipper"] = cells[15].text.strip()
                # Get knowledge destination port
                knowledge["destination"] = cells[17].text.strip()
                # Get knowledge transmission date
                try:
                    text = cells[20].text.strip()
                    knowledge["transmission"] = datetime.strptime(text, "%d/%m/%Y %I:%M:%S %p") - timedelta(hours = -5)
                except IndexError:
                    logging.info("Could not find transmission date for knowledge {0[code]}".format(knowledge))
                except ValueError:
                    logging.info("Could not parse transmission date {0} for knowledge {1[code]}".format(text, knowledge))
                # Upsert knowledge
                query = {
                    "manifest": knowledge["manifest"],
                    "detail": knowledge["detail"]
                }
                update = {
                    "$set": knowledge
                }
                knowledges.update_one(query, update, upsert=True)
        except:
            logging.info("Manifest {0[year]}-{0[code]} has no knowledges".format(manifest))

        # Update manifest
        manifest["processed"] = True
        query = {
            "_id": manifest["_id"]
        }
        update = {
            "$set": manifest
        }
        manifests.update_one(query, update)
except:
    logging.exception("Unknown exception occurred")
finally:
    driver.quit()
