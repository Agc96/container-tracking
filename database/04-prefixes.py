#!/usr/bin/env python3

from pymongo import MongoClient

# Initialize MongoDB
mongo = MongoClient()
dbold = mongo.scraper2
dbnew = mongo.tracking

for prefix in dbold.container_prefixes.find():
    # Discretize carrier
    carrier = 0
    if prefix["carrier"] == "Maersk":
        carrier = 1
    elif prefix["carrier"] == "Hapag-Lloyd":
        carrier = 2
    elif prefix["carrier"] == "Evergreen":
        carrier = 3
    elif prefix["carrier"] == "Textainer":
        carrier = 4
    else:
        print("Found unknown carrier {0}".format(prefix["carrier"]))
    # Insert prefix
    dbnew.prefixes.insert_one({
        "code": prefix["prefix"],
        "carrier": carrier
    })
